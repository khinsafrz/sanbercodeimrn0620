//Khinsa tugas 2
//if else
var nama = "Khinsa"
var peran = "Werewolf"
if ( nama !== "" && peran == "") {
    console.log("Halo ".concat(nama, ', Pilih peranmu untuk memulai game!'))
} else if ( peran == "Penyihir" ) {
    console.log("Selamat datang di Dunia Werewolf, ".concat(nama))
    console.log("Halo ".concat(peran, ' ', nama, ', kamu dapat melihat siapa yang menjadi werewolf!'))
}else if ( peran == "Guard" ) {
        console.log("Selamat datang di Dunia Werewolf, ".concat(nama))
        console.log("Halo ".concat(peran, ' ', nama, ', kamu akan membantu melindungi temanmu dari serangan werewolf.'))
} else if ( peran == "Werewolf" ) {
    console.log("Selamat datang di Dunia Werewolf, ".concat(nama))
    console.log("Halo ".concat(peran, ' ', nama, ', Kamu akan memakan mangsa setiap malam!'))
}else {
    console.log("Nama harus diisi!")
}

//switch case
var tanggal = '21'; 
var bulan = 1; 
var tahun = '1945';
switch(bulan){
    case 1: {console.log(tanggal.concat(' Januari ', tahun)); break;}
    case 2: {console.log(tanggal.concat(' Februari ', tahun)); break;}
    case 3: {console.log(tanggal.concat(' Maret' , tahun)); break;}
    case 4: {console.log(tanggal.concat(' April ', tahun)); break;}
    case 5: {console.log(tanggal.concat(' Mei ', tahun)); break;}
    case 6: {console.log(tanggal.concat(' Juni ', tahun)); break;}
    case 7: {console.log(tanggal.concat(' Juli ', tahun)); break;}
    case 8: {console.log(tanggal.concat(' Agustus ', tahun)); break;}
    case 9: {console.log(tanggal.concat(' September ', tahun)); break;}
    case 10: {console.log(tanggal.concat(' Oktober ', tahun)); break;}
    case 11: {console.log(tanggal.concat(' November ', tahun)); break;}
    case 12: {console.log(tanggal.concat(' Desember ', tahun)); break;}
}