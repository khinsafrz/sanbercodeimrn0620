//no.1
console.log('LOOPING PERTAMA')
var angka = 2;
while(angka < 21) {
  console.log(angka + ' - I love coding'); 
  angka +=2;
}
console.log('LOOPING KEDUA')
var angka = 20;
while(angka > 1) {
  console.log(angka + ' - I will become a mobile developer'); 
  angka -=2;
}

//no.2
for(var x = 1; x < 21; x ++) {
    if (x % 3 == 0 && x % 2 !== 0) {
        console.log(x +  " - I Love Coding");
}
else if (x % 2 == 0) {
        console.log(x + " - Berkualitas");   
}
else {
        console.log(x + " - Santai");
}
  }

//no. 3
rows=8
columns=4
for(i = 1; i <= rows; i++){
	for(j = 1; j <= columns; j++){
		console.log('#'); 
	}
	console.log("\n"); 
    }	
    
//no.4
for(var a=1; a<=7; a++){
  console.log("#".repeat(a));
}

//no.5
var row = 0;
	var cols = 0;
	while(row <= 8 ){
	  flag = row % 2;
	  while(cols <= 8){
	    if (flag) {console.log( "# "); }
	         else {console.log( " #"); }
	    cols++;
	  console.log(' ');
	  cols = 0;
	  row++;
	}}