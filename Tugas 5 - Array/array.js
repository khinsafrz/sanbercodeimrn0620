//no. 1
function range(startNum, finishNum) {
    var ans = [];
    if (arguments.length !== 2) {
        return('-1')
            }
    for (let i = startNum; i <= finishNum; i++) {
        ans.push(i);
    }
    for (let i = startNum; i >= finishNum; i--) {
        ans.push(i);
    }
    return(ans)
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

//no.2
function rangeWithStep(startNum, finishNum, step) {
    let arr = [];
    for(let i = startNum; i <= finishNum; i += step){
       arr.push(i);
    }
    for(let i = finishNum; i <= startNum; i += step){
        arr.push(i);
    }
    return arr;
}
 
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

//no.3
function sum(startNum, finishNum, step) {
    var x = [];
    if(step === undefined)
      step = 1;
        if (startNum == null) {
            return (0);
        } 
        if (step > 0) {
            for (var y = startNum; y <= finishNum; y += step) {
                x.push(y)
            }
        } else {
            for (var z = startNum; z >= finishNum; z += step) {
                x.push(z)
            }
        }
        return x.reduce ((a, b) => a + b, 0);
      }
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//no. 4
function dataHandling(input){
    for (let i = 0; i < input.length; i++) {
      var innerArrayLength = input[i].length;
      for (let j = 0; j < innerArrayLength; j++) {
          console.log('Nomor ID: ' + input[j][0]);
          console.log('Nama Lengkap: ' + input[j][1]);
          console.log('TTL: ' + input[j][2] + ' ' + input[j][3]);
          console.log('Hobi: ' + input[j][4]);
      }
  }
  }
  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ] 
  console.log(dataHandling(input))

//no.5
function balikKata(s){
    var o = '';
    for (var i = s.length - 1; i >= 0; i--)
      o += s[i];
    return o;
  }
  
  console.log(balikKata("Kasur Rusak")) // kasuR rusaK
  console.log(balikKata("SanberCode")) // edoCrebnaS
  console.log(balikKata("Haji Ijah")) // hajI ijaH
  console.log(balikKata("racecar")) // racecar
  console.log(balikKata("I am Sanbers")) // srebnaS ma I 

  //no.6
  function dataHandling2(input){
    input.splice(1,4, 'Roman Alamsyah Elsharawy', 'Provinsi Bandar Lampung', "21/05/1989", "Pria", "SMA Internasional Metro");
      console.log(input);
    let tgl = input.slice(3,4);
    console.log(tgl);
    let tglsplit = tgl.split('/');
    console.log(tglsplit);
  //maaf belum selesai
  }
  var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
  console.log(dataHandling2(input));